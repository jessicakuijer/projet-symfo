# Projet Symfo

Projet symfony avec CI, Docker à propos de...

## Environnement de développement

### Pré-requis

* PHP 7.4
* composer
* Symfony CLI
* Docker
* Docker-compose

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
docker-compose up -d
symfony serve -d
```